<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Helper\StoreLocator;

use Mastering\StoreLocator\Api\Data\PageHelperInterface;
use Mastering\StoreLocator\Helper\Data;
use Magento\Framework\UrlInterface;

/**
 * Helper class for pages
 */
class PageHelper implements PageHelperInterface
{

    /**
     * @var Data
     */
    public Data $data;

    /**
     * @var UrlInterface
     */
    public UrlInterface $url;

    /**
     * @param Data $data
     * @param UrlInterface $url
     */
    public function __construct(
        Data $data,
        UrlInterface $url
    ) {
        $this->data = $data;
        $this->url = $url;
    }

    /**
     * @param $redirect
     * @param $response
     * @return void
     */
    public function checkModuleEnableFrontend($redirect, $response)
    {
        $isModuleEnable = $this->isModuleEnable();
        if($isModuleEnable){
            $this->redirectTo404($redirect, $response);
        }
    }

    /**
     * @param $redirect
     * @param $response
     * @return void
     */
    public function checkModuleEnableAdmin($redirect, $response)
    {
        $isModuleEnable = $this->isModuleEnable();
        if($isModuleEnable){
            $this->redirectToConfig($redirect, $response);
        }
    }

    /**
     * @param $redirect
     * @param $response
     * @return void
     */
    public function redirectTo404($redirect, $response)
    {
        $noRouteUrl = $this->url->getUrl('404');
        $redirect->redirect($response, $noRouteUrl);
    }

    /**
     * @param $redirect
     * @param $response
     * @return void
     */
    public function redirectToConfig($redirect, $response)
    {
        $noRouteUrl = $this->url->getUrl(PageHelperInterface::ADMIN_CONFIG);
        $redirect->redirect($response, $noRouteUrl);
    }

    /**
     * @return bool
     */
    private function isModuleEnable(): bool
    {
        return $this->data->moduleIsEnable();
    }
}
