<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Helper\StoreLocator;
use Magento\Framework\Exception\NoSuchEntityException;
use Mastering\StoreLocator\Api\Data\ImageInterface;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Helper\Data;

/**
 * Helper class for saving data
 */
class DataSave
{

    /**
     * @var StoreLocatorInterface
     */
    private StoreLocatorInterface $model;

    /**
     * @var Data
     */
    private Data $dataHelper;

    /**
     * @param StoreLocatorInterface $model
     * @param Data $dataHelper
     */
    public function __construct(
        StoreLocatorInterface $model,
        Data $dataHelper
    )
    {
        $this->model = $model;
        $this->dataHelper = $dataHelper;
    }


    /**
     * @param array|null $data
     * @return void
     * @throws NoSuchEntityException
     */
    public function saveData(array $data = null)
    {
        if (is_null($data)) {
            $storeName = $this->model->getStoreName();
            $description = $this->model->getDescription();
            $image = $this->model->getImage();
            $country = $this->model->getCountry();
            $city = $this->model->getCity();
            $address = $this->model->getAddress();
            $schedule = $this->model->getSchedule();
        } else {
            $storeName = $data[StoreLocatorInterface::STORE_NAME] ?? "";
            $description = $data[StoreLocatorInterface::DESCRIPTION] ?? "";
            $image = $data[StoreLocatorInterface::IMAGE] ?? null;
            $country = $data[StoreLocatorInterface::COUNTRY] ?? "";
            $city = $data[StoreLocatorInterface::CITY] ?? "";
            $address = $data[StoreLocatorInterface::ADDRESS] ?? "";
            $schedule = $data[StoreLocatorInterface::SCHEDULE] ?? "";
        }

        $this->saveStoreName($storeName);
        $this->saveDescription($description);
        $this->saveCountry($country);
        $this->saveCity($city);
        $this->saveAddress($address);
        $this->saveSchedule($schedule);
        $this->setImage($image);
        $this->saveUrlKey();
        $this->saveCoordinates();
    }

    /**
     * @param string $storeName
     * @return void
     * @throws NoSuchEntityException
     */
    private function saveStoreName(string $storeName): void
    {
        if ($storeName === ""){
            throw new NoSuchEntityException(__('Store name is null'));
        }else {
            $this->model->setStoreName($storeName);
        }
    }

    /**
     * @param string $description
     * @return void
     */
    private function saveDescription(string $description): void
    {
        $this->model->setDescription($description);
    }

    /**
     * @param string $image
     * @return void
     */
    private function saveImage(string $image): void
    {
        if (strpos($image, ImageInterface::IMAGE_PATH) === false) {
            $imagePath = ImageInterface::IMAGE_PATH . $image;
            $this->model->setImage($imagePath);
        }
    }

    /**
     * @param $image
     * @return void
     */
    private function setImage($image): void
    {
        if (!is_null($image)) {
            if (isset( $image[0]['name'])) {
                $image = $image[0]['name'];
            }
            $this->saveImage($image);
        } else {
            $this->model->setImage($this->dataHelper->getStandardImage());
        }
        if ($image === "") {
            $this->model->setImage($this->dataHelper->getStandardImage());
        }
    }

    /**
     * @param string $country
     * @return void
     */
    private function saveCountry(string $country): void
    {
        $this->model->setCountry($country);
    }

    /**
     * @param string $city
     * @return void
     */
    private function saveCity(string $city): void
    {
        $this->model->setCity($city);
    }

    /**
     * @param string $address
     * @return void
     */
    private function saveAddress(string $address): void
    {
        $this->model->setAddress($address);
    }

    /**
     * @param string $schedule
     * @return void
     */
    private function saveSchedule(string $schedule): void
    {
        $this->model->setSchedule($schedule);
    }

    /**
     * @return void
     */
    private function saveUrlKey(): void
    {
        $urlKey = $this->model->getUrlKey();
        if ($urlKey === "") {
            $urlKey =  $this->randString(4) . "-" . $this->model->getStoreName();

            $divider = '_';
            $urlKey = preg_replace('~[^\pL\d]+~u', $divider, $urlKey);
            $urlKey = iconv('utf-8', 'us-ascii//TRANSLIT', $urlKey);
            $urlKey = preg_replace('~[^-\w]+~', '', $urlKey);
            $urlKey = trim($urlKey, $divider);
            $urlKey = preg_replace('~-+~', $divider, $urlKey);
            $urlKey = strtolower($urlKey);

            $this->model->setUrlKey($urlKey);
        }
    }

    /**
     * @param int $length
     * @return string
     */
    private function randString(int $length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    /**
     * @return string
     */
    private function getAddress(): string
    {
        $address = $this->model->getAddress();
        $city = $this->model->getCity();
        $country = $this->model->getCountry();
        $address = "{$address}, {$city}, {$country}";
        $address = urlencode($address);

        return $address;
    }

    /**
     * @return void
     * @throws NoSuchEntityException
     */
    function saveCoordinates(): void
    {
        $apiKey = $this->dataHelper->getApiKey();

        $address = $this->getAddress();

        $jsonData = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$apiKey}&isAjax=true");
        $addressData = json_decode($jsonData, true);
        $requestStatus = strcmp($addressData['status'], 'OK') === 0;
        if ($requestStatus)  {
            $addressData = end($addressData['results']);

            if (strcmp($addressData['geometry']['location_type'], 'RANGE_INTERPOLATED') == 0 ) {
                throw new NoSuchEntityException(__('Address not valid'));
            }
            if ($this->correctAddressType($addressData['types'][0])) {
                $isCorrectCoordinates = isset($addressData['geometry']['location']);
                if ($isCorrectCoordinates) {
                    $cordsData = $addressData['geometry']['location'];
                    $this->model->setLongitude($cordsData['lng']);
                    $this->model->setLatitude($cordsData['lat']);
                } else {
                    throw new NoSuchEntityException(__('Address not valid'));
                }
            }
        } else {
            throw new NoSuchEntityException(__('Address not valid'));
        }
    }

    /**
     * @param string $addressType
     * @return bool
     * @throws NoSuchEntityException
     */
    private function correctAddressType(string $addressType): bool
    {
        $errorTypes = [
            'locality',
            'neighborhood'
        ];

        foreach ($errorTypes as $type){
            if (strcmp($addressType, $type) === 0 ) {
                throw new NoSuchEntityException(__('Address not valid'));
            }
        }
        return true;
    }
}
