<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Mastering\StoreLocator\Api\Data\ImageInterface;

/**
 * Helper class for getting data from system configuration
 */
class Data extends AbstractHelper
{
    const XML_PATH_RSGITECH_NEWS = 'mastering_storeLocator/';

    /**
     * @param $field
     * @param $storeCode
     * @return mixed
     */
    private function getConfigValue($field, $storeCode = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_STORE, $storeCode);
    }

    /**
     * @param $fieldid
     * @param $storeCode
     * @return mixed
     */
    private function getGeneralConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH_RSGITECH_NEWS.'general/'.$fieldid, $storeCode);
    }

    /**
     * @param $fieldid
     * @param $storeCode
     * @return mixed
     */
    private function getApiConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH_RSGITECH_NEWS.'api/'.$fieldid, $storeCode);
    }

    /**
     * @param $fieldid
     * @param $storeCode
     * @return mixed
     */
    private function getStandatdConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH_RSGITECH_NEWS.'standard/'.$fieldid, $storeCode);
    }

    /**
     * @param $storeCode
     * @return mixed
     */
    public function getApiKey($storeCode = null)
    {
        return $this->getApiConfig('api', $storeCode);
    }

    /**
     * @param $storeCode
     * @return mixed
     */
    public function moduleIsEnable($storeCode = null)
    {
        return !$this->getGeneralConfig('enable', $storeCode);
    }

    /**
     * @param $storeCode
     * @return string
     */
    public function getStandardImage($storeCode = null)
    {
        $imagePath = ImageInterface::IMAGE_PATH . $this->getStandatdConfig('image_upload', $storeCode);
        return $imagePath;
    }
}
