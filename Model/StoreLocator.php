<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model;

use Magento\Framework\Model\AbstractModel;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;

/**
 * Store locator model
 */
class StoreLocator extends AbstractModel implements StoreLocatorInterface
{
    /**
     * @return void
     */
    public function _construct()
    {
        parent::_init(StoreLocatorResource::class);
    }

    /**
     * Get id attribute
     *
     * @return mixed|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * Get id attribute
     *
     * @param mixed $value
     * @return $this
     */
    public function setId($value): StoreLocatorInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }


    /**
     * Get store name attribute
     *
     * @return string
     */
    public function getStoreName(): string
    {
        return $this->getData(self::STORE_NAME) ?? "";
    }

    /**
     * Set store name attribute
     *
     * @param string $storeName
     * @return void
     */
    public function setStoreName(string $storeName): void
    {
        $this->setData(self::STORE_NAME, $storeName);
    }

    /**
     * Get description attribute
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getData(self::DESCRIPTION) ?? "";

    }

    /**
     * Set description attribute
     *
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get image attribute
     *
     * @return string
     */
    public function getImage(): string
    {
        return $this->getData(self::IMAGE) ?? "";
    }

    /**
     * Set image attribute
     *
     * @param string $image
     * @return void
     */
    public function setImage(string $image): void
    {
        $this->setData(self::IMAGE, $image);
    }

    /**
     * Get country attribute
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->getData(self::COUNTRY) ?? "";
    }

    /**
     * Set country attribute
     *
     * @param string $country
     * @return void
     */
    public function setCountry(string $country): void
    {
        $this->setData(self::COUNTRY, $country);
    }

    /**
     * Get city attribute
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->getData(self::CITY) ?? "";
    }

    /**
     * Set city attribute
     *
     * @param string $city
     * @return void
     */
    public function setCity(string $city): void
    {
        $this->setData(self::CITY, $city);
    }

    /**
     * Get address attribute
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->getData(self::ADDRESS) ?? "";
    }

    /**
     * Set address attribute
     *
     * @param string $address
     * @return void
     */
    public function setAddress(string $address): void
    {
        $this->setData(self::ADDRESS, $address);
    }

    /**
     * Get schedule attribute
     *
     * @return string
     */
    public function getSchedule(): string
    {
        return $this->getData(self::SCHEDULE) ?? "";
    }

    /**
     * Set schedule attribute
     *
     * @param string $schedule
     * @return void
     */
    public function setSchedule(string $schedule): void
    {
        $this->setData(self::SCHEDULE, $schedule);
    }

    /**
     * Get longitude attribute
     *
     * @return float
     */
    public function getLongitude(): float
    {
        return (float)$this->getData(self::LONGITUDE) ?? 0;
    }

    /**
     * Set longitude attribute
     *
     * @param float $longitude
     * @return void
     */
    public function setLongitude(float $longitude): void
    {
        $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * Get latitude attribute
     *
     * @return float
     */
    public function getLatitude(): float
    {
        return (float)$this->getData(self::LATITUDE) ?? 0;
    }

    /**
     * Set latitude attribute
     *
     * @param float $latitude
     * @return void
     */
    public function setLatitude(float $latitude): void
    {
        $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * Get store url attribute
     *
     * @return string
     */
    public function getUrlKey(): string
    {
        return $this->getData(self::URL_KEY) ?? "";
    }

    /**
     * Set store url attribute
     *
     * @param string $urlKey
     * @return void
     */
    public function setUrlKey(string $urlKey): void
    {
        $this->setData(self::URL_KEY, $urlKey);
    }
}
