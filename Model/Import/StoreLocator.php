<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Import;

use Exception;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\ImportExport\Helper\Data as ImportHelper;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\Entity\AbstractEntity;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Magento\ImportExport\Model\ResourceModel\Helper;
use Magento\ImportExport\Model\ResourceModel\Import\Data;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Mastering\StoreLocator\Helper\StoreLocator\DataSave;
use Mastering\StoreLocator\Model\StoreLocatorFactory;
use Mastering\StoreLocator\Helper\Data as DataHelper;

/**
 * Import model
 */
class StoreLocator extends AbstractEntity
{
    const ENTITY_CODE = 'storeLocator_import';

    /**
     * table name in db
     */
    const TABLE = 'mastering_store_locator';

    /**
     * Column names
     */
    private const ENTITY_ID_COLUMN = StoreLocatorInterface::ID;

    private const STORE_NAME =  StoreLocatorInterface::STORE_NAME;

    private const DESCRIPTION = StoreLocatorInterface::DESCRIPTION;

    private const COUNTRY = StoreLocatorInterface::COUNTRY;

    private const CITY = StoreLocatorInterface::CITY;

    private const ADDRESS = StoreLocatorInterface::ADDRESS;

    private const IMAGE = StoreLocatorInterface::IMAGE;

    /**
     * If we should check column names
     */
    protected $needColumnCheck = true;

    /**
     * Need to log in import history
     */
    protected $logInHistory = true;

    /**
     * Permanent entity columns.
     */
    protected $_permanentAttributes = [
        self::ENTITY_ID_COLUMN
    ];

    /**
     * Valid column names
     */
    protected $validColumnNames = [
        self::ENTITY_ID_COLUMN,
        self::STORE_NAME,
        self::DESCRIPTION,
        self::COUNTRY,
        self::CITY,
        self::ADDRESS,
        self::IMAGE
    ];

    /**
     * @var AdapterInterface
     */
    protected AdapterInterface $connection;

    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var DataHelper
     */
    private DataHelper $dataHelper;

    /**
     * @param JsonHelper $jsonHelper
     * @param ImportHelper $importExportData
     * @param Data $importData
     * @param Helper $resourceHelper
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param DataHelper $dataHelper
     */
    public function __construct(
        JsonHelper $jsonHelper,
        ImportHelper $importExportData,
        Data $importData,
        Helper $resourceHelper,
        ProcessingErrorAggregatorInterface $errorAggregator,
        StoreLocatorRepository $storeLocatorRepository,
        StoreLocatorFactory $storeLocatorFactory,
        DataHelper $dataHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->errorAggregator = $errorAggregator;
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode(): string
    {
        return static::ENTITY_CODE;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    public function getValidColumnNames(): array
    {
        return $this->validColumnNames;
    }

    /**
     * Row validation
     *
     * @param array $rowData
     * @param int $rowNum
     *
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum): bool
    {
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Import data
     *
     * @return bool
     *
     * @throws Exception
     */
    protected function _importData(): bool
    {
        switch ($this->getBehavior()) {
            case Import::BEHAVIOR_DELETE:
                $this->deleteEntity();
                break;
            case Import::BEHAVIOR_REPLACE:
                $this->saveAndReplaceEntity();
                break;
            case Import::BEHAVIOR_APPEND:
                $this->saveAndReplaceEntity();
                break;
        }
        return true;
    }

    /**
     * Delete entities
     *
     * @return bool
     */
    private function deleteEntity(): bool
    {
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);

                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowId = $rowData[static::ENTITY_ID_COLUMN];
                    $rows[] = $rowId;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }

        if ($rows) {
            return $this->deleteEntityFinish(array_unique($rows));
        }
        return false;
    }

    /**
     * Save and replace entities
     *
     * @return void
     */
    private function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $rows = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];

            foreach ($bunch as $rowNum => $row) {
                if (!$this->validateRow($row, $rowNum)) {
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }
                $rowId = $row[static::ENTITY_ID_COLUMN];
                $rows[] = $rowId;
                $columnValues = [];
                foreach ($this->getAvailableColumns() as $columnKey) {
                    $columnValues[$columnKey] = $row[$columnKey];
                }

                $entityList[$rowId][] = $columnValues;
            }

            if (Import::BEHAVIOR_REPLACE === $behavior) {
                if ($rows && $this->deleteEntityFinish(array_unique($rows))) {
                    $this->saveEntityFinish($entityList);
                }
            } elseif (Import::BEHAVIOR_APPEND === $behavior) {
                $this->saveEntityFinish($entityList);
            }
        }
    }

    /**
     * Save entities
     *
     * @param array $entityData
     *
     * @return bool
     */
    private function saveEntityFinish(array $entityData): bool
    {
        if ($entityData) {
            $rows = [];
            foreach ($entityData as $entityRows) {
                foreach ($entityRows as $row) {
                    $rows[] = $row;
                }
            }
            if ($rows) {
                foreach ($rows as $row){
                    try {
                        $id = (int)$row[self::ENTITY_ID_COLUMN];
                        $storeLocator = $this->storeLocatorRepository->get($id);
                        $this->countItemsUpdated += 1;
                    } catch (\Exception $e) {
                        $storeLocator = $this->storeLocatorFactory->create();
                    }
                    try {
                        $dataSave = new DataSave($storeLocator, $this->dataHelper);
                        $dataSave->saveData($row);
                        $this->storeLocatorRepository->save($storeLocator);
                        $this->countItemsCreated += 1;
                    } catch (\Exception $exception) {
                        continue;
                    }
                }
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Delete entities
     *
     * @param array $entityIds
     *
     * @return bool
     */
    private function deleteEntityFinish(array $entityIds): bool
    {
        if ($entityIds) {
            foreach ($entityIds as $entityId){
                try {
                    $this->storeLocatorRepository->deleteById($entityId);
                } catch (Exception $e) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get available columns
     *
     * @return array
     */
    private function getAvailableColumns(): array
    {
        return $this->validColumnNames;
    }
}
