<?php

namespace Mastering\StoreLocator\Model\ResourceModel\StoreLocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Mastering\StoreLocator\Model\StoreLocator;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Collection class for StoreLocator model
 */
class Collection extends AbstractCollection
{
    /**
     * DB id field name
     */
    protected $_idFieldName = StoreLocatorInterface::ID;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(StoreLocator::class, StoreLocatorResource::class);
    }
}
