<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Store locator db connection
 */
class StoreLocator extends AbstractDb
{

    /**
     * DB table name
     */
    const TABLE_NAME = 'mastering_store_locator';

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, StoreLocatorInterface::ID);
    }
}
