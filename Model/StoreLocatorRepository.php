<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\StateException;
use Mastering\StoreLocator\Helper\Data;
use Magento\Setup\Exception;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Mastering\StoreLocator\Api\StoreLocatorSearchResultInterface;
use Mastering\StoreLocator\Api\StoreLocatorSearchResultInterfaceFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Filesystem;
use Mastering\StoreLocator\Helper\StoreLocator\DataSave;

class StoreLocatorRepository implements StoreLocatorRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var Data
     */
    private Data $dataHelper;

    /**
     * @var File
     */
    protected File $fileIo;

    /**
     * @var Filesystem
     */
    protected Filesystem $filesystem;

    /**
     * @var StoreLocatorSearchResultInterfaceFactory
     */
    private StoreLocatorSearchResultInterfaceFactory $searchResultFactory;

    /**
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param CollectionFactory $collectionFactory
     * @param StoreLocatorResource $storeLocatorResource
     * @param StoreLocatorSearchResultInterfaceFactory $storeLocatorSearchResultInterfaceFactory
     * @param Data $dataHelper
     * @param Filesystem $filesystem
     * @param File $fileIo
     */
    public function __construct(
        StoreLocatorFactory                      $storeLocatorFactory,
        CollectionFactory                        $collectionFactory,
        StoreLocatorResource                     $storeLocatorResource,
        StoreLocatorSearchResultInterfaceFactory $storeLocatorSearchResultInterfaceFactory,
        Data $dataHelper,
        Filesystem $filesystem,
        File $fileIo
    ) {
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->collectionFactory = $collectionFactory;
        $this->storeLocatorResource = $storeLocatorResource;
        $this->searchResultFactory = $storeLocatorSearchResultInterfaceFactory;
        $this->dataHelper = $dataHelper;
        $this->filesystem = $filesystem;
        $this->fileIo = $fileIo;
    }

    /**
     * @param mixed $id
     * @return StoreLocatorInterface
     * @throws NoSuchEntityException
     */
    public function get($id): StoreLocatorInterface
    {
        $object = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($object, (int)$id);
        if (! $object->getId()) {
            throw new NoSuchEntityException(__('Unable to find entity with ID "%1"', $id));
        }
        return $object;
    }

    /**
     * @param string $urlKey
     * @return StoreLocatorInterface
     * @throws NoSuchEntityException
     */
    public function getByUrlKey(string $urlKey): StoreLocatorInterface
    {
        $object = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($object, $urlKey, StoreLocatorInterface::URL_KEY);
        if (! $object->getId()) {
            throw new NoSuchEntityException(__('Unable to find entity with url key "%1"', $urlKey));
        }
        return $object;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): StoreLocatorSearchResultInterface
    {
        $collection = $this->collectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param StoreLocatorInterface $storeLocator
     * @return StoreLocatorInterface
     * @throws AlreadyExistsException
     * @throws Exception
     */
    public function save(StoreLocatorInterface $storeLocator): StoreLocatorInterface
    {
        try {
            $dataSave = new DataSave($storeLocator, $this->dataHelper);
            $dataSave->saveData();
        } catch (Exception $e) {
            throw new $e;
        }
        $this->storeLocatorResource->save($storeLocator);
        return $storeLocator;
    }

    /**
     * @param StoreLocatorInterface $workingHours
     * @return bool
     * @throws StateException
     */
    public function delete(StoreLocatorInterface $workingHours): bool
    {
        try {
            $imagePath = $workingHours->getImage();
            $this->storeLocatorResource->delete($workingHours);
            $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
            $this->fileIo->rm('/var/www/magento2/pub/media/' . $imagePath);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove entity #%1', $workingHours->getId()));
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById($id): bool
    {
        try {
            return $this->delete($this->get((int)$id));
        } catch (NoSuchEntityException $e) {
            throw new $e;
        }

    }
}

