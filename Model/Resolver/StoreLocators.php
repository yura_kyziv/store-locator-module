<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Mastering\StoreLocator\Model\StoreLocator as StoreLocatorModel;

class StoreLocators implements ResolverInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|array[]
     */
    public function resolve(
        Field $field,
              $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null)
    {
        $collection = $this->collectionFactory->create();

        if (empty($collection)) {
            return [];
        }

        $items = [];

        /** @var StoreLocatorModel $storeLocator */
        foreach ($collection->getItems() as $storeLocator) {
            $items[] = [
                'id' => $storeLocator->getId(),
                'store_name' => $storeLocator->getStoreName(),
                'description' => $storeLocator->getDescription(),
                'image' => $storeLocator->getImage(),
                'country' => $storeLocator->getCountry(),
                'city' => $storeLocator->getCity(),
                'address' => $storeLocator->getAddress(),
                'schedule' => $storeLocator->getSchedule(),
                'longitude' => $storeLocator->getLongitude(),
                'latitude' => $storeLocator->getLatitude(),
                'url_key' => $storeLocator->getUrlKey(),
            ];
        }

        return ['items' => $items];
    }
}
