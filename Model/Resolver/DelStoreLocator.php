<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Mastering\StoreLocator\Model\StoreLocatorRepository;

class DelStoreLocator implements ResolverInterface
{
    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository
    )
    {
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|Value|mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function resolve(
        Field $field,
              $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $id = $this->getStoreLocatorId($args);
        $result = $this->deleteStoreLocatorData($id);
        return $result;
    }

    /**
     * @param array $args
     * @return int
     * @throws GraphQlInputException
     */
    private function getStoreLocatorId(array $args): int
    {
        if (!isset($args['id'])) {
            throw new GraphQlInputException(__('Store Locator not find'));
        }
        return (int)$args['id'];
    }
    /**
     * @param int $id
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    private function deleteStoreLocatorData(int $id): array
    {
        try {
            $this->storeLocatorRepository->deleteById($id);
            $item = [
                'success' => true,
            ];

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $item;
    }
}