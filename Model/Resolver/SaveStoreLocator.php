<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Mastering\StoreLocator\Helper\StoreLocator\DataSave;
use Mastering\StoreLocator\Model\StoreLocatorFactory;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Mastering\StoreLocator\Helper\Data;
use Exception;

class SaveStoreLocator implements ResolverInterface
{
    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var Data
     */
    private Data $dataHelper;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param Data $dataHelper
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository,
        StoreLocatorFactory $storeLocatorFactory,
        Data $dataHelper
    )
    {
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|Value|mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function resolve(
        Field $field,
              $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        $input = $this->getStoreLocatorParams($args);
        $result = $this->saveStoreLocatorData($input);
        return $result;
    }

    /**
     * @param array $args
     * @return array
     * @throws GraphQlInputException
     */
    private function getStoreLocatorParams(array $args): array
    {
        if (!isset($args['storeLocator'])) {
            throw new GraphQlInputException(__('Store Locator not find'));
        }
        return $args['storeLocator'];
    }

    /**
     * @param $input
     * @return array
     * @throws GraphQlNoSuchEntityException
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Setup\Exception
     */
    private function saveStoreLocatorData($input): array
    {
        try {
            $id = $input['id'];
            $storeLocator = $this->storeLocatorRepository->get($id);
            $saveType = 'update';
        } catch (Exception $e) {
            $storeLocator = $this->storeLocatorFactory->create();
            $saveType = 'create';
        }

        try {
            $dataSave = new DataSave($storeLocator, $this->dataHelper);
            $dataSave->saveData($input);

        } catch (Exception $exception) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        $this->storeLocatorRepository->save($storeLocator);
        return [
            'output' => [
                'success' => true,
            ],
            'type' => $saveType,
            'result' => [
                'id' => $storeLocator->getId(),
                'store_name' => $storeLocator->getStoreName(),
                'description' => $storeLocator->getDescription(),
                'image' => $storeLocator->getImage(),
                'country' => $storeLocator->getCountry(),
                'city' => $storeLocator->getCity(),
                'address' => $storeLocator->getAddress(),
                'schedule' => $storeLocator->getSchedule(),
                'longitude' => $storeLocator->getLongitude(),
                'latitude' => $storeLocator->getLatitude(),
                'url_key' => $storeLocator->getUrlKey(),
            ]

        ];
    }
}