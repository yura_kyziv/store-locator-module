<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Mastering\StoreLocator\Model\StoreLocatorRepository;

class StoreLocator implements ResolverInterface
{
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository
    )
    {
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|Value|mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function resolve(
        Field $field,
              $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $id = $this->getStoreLocatorId($args);
        $storeLocatorItem = $this->getStoreLocatorData($id);
        return $storeLocatorItem;
    }

    /**
     * @param array $args
     * @return int
     * @throws GraphQlInputException
     */
    private function getStoreLocatorId(array $args): int
    {
        if (!isset($args['id'])) {
            throw new GraphQlInputException(__('Store Locator not find'));
        }
        return (int)$args['id'];
    }
    /**
     * @param int $id
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    private function getStoreLocatorData(int $id): array
    {
        $item = [];
        try {
            $storeLocator = $this->storeLocatorRepository->get($id);

            $item = [
                'id' => $storeLocator->getId(),
                'store_name' => $storeLocator->getStoreName(),
                'description' => $storeLocator->getDescription(),
                'image' => $storeLocator->getImage(),
                'country' => $storeLocator->getCountry(),
                'city' => $storeLocator->getCity(),
                'address' => $storeLocator->getAddress(),
                'schedule' => $storeLocator->getSchedule(),
                'longitude' => $storeLocator->getLongitude(),
                'latitude' => $storeLocator->getLatitude(),
                'url_key' => $storeLocator->getUrlKey(),
            ];

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $item;
    }
}