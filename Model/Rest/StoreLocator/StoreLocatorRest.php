<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Rest\StoreLocator;

use Mastering\StoreLocator\Model\StoreLocatorRepository;

/**
 * Class for store locator rest
 */
abstract class StoreLocatorRest
{

    /**
     * @var StoreLocatorRepository
     */
    protected StoreLocatorRepository $storeLocatorRepository;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository
    ){
        $this->storeLocatorRepository = $storeLocatorRepository;
    }
}
