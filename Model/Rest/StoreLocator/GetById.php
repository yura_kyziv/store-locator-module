<?php
declare(strict_types=1);
namespace Mastering\StoreLocator\Model\Rest\StoreLocator;

use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Api\Rest\StoreLocator\GetByIdInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Rest store locator get by id action
 */
class GetById extends StoreLocatorRest implements GetByIdInterface
{

    /**
     * @param $id
     * @return StoreLocatorInterface
     * @throws NoSuchEntityException
     */
    public function getById($id): StoreLocatorInterface
    {
        return $this->storeLocatorRepository->get($id);
    }
}
