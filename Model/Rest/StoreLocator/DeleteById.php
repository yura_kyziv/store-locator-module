<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Rest\StoreLocator;

use Mastering\StoreLocator\Api\Rest\StoreLocator\DeleteByIdInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Rest store locator delete by id action
 */
class DeleteById extends StoreLocatorRest implements DeleteByIdInterface
{

    /**
     * @param $id
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById($id): bool
    {
        return $this->storeLocatorRepository->deleteById($id);
    }
}
