<?php
declare(strict_types=1);
namespace Mastering\StoreLocator\Model\Rest\StoreLocator;

use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Api\Rest\StoreLocator\SaveInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Setup\Exception;

/**
 * Rest store locator save action
 */
class Save extends StoreLocatorRest implements SaveInterface
{

    /**
     * @param StoreLocatorInterface $storeLocator
     * @return StoreLocatorInterface
     * @throws AlreadyExistsException
     * @throws Exception
     */
    public function save(StoreLocatorInterface $storeLocator): StoreLocatorInterface
    {
        return $this->storeLocatorRepository->save($storeLocator);
    }
}
