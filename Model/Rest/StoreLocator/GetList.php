<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model\Rest\StoreLocator;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Mastering\StoreLocator\Api\Rest\StoreLocator\GetListInterface;
use Mastering\StoreLocator\Api\StoreLocatorSearchResultInterface;
use Mastering\StoreLocator\Model\StoreLocatorRepository;

/**
 * Rest store locator get items list action
 */
class GetList extends StoreLocatorRest implements GetListInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        parent::__construct($storeLocatorRepository);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(): StoreLocatorSearchResultInterface
    {
        $filter = $this->searchCriteriaBuilder->setCurrentPage(1)->create();
        return $this->storeLocatorRepository->getList($filter);
    }
}
