<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Model;

use Mastering\StoreLocator\Api\StoreLocatorSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class StoreLocatorSearchResult extends SearchResults implements StoreLocatorSearchResultInterface
{

}
