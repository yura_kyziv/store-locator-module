<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\ViewModel;

use Magento\Framework\App\Filesystem\DirectoryList;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Mastering\StoreLocator\Helper\Data;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Model for view pages
 */
class StoreLocatorViewModel implements ArgumentInterface
{
    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var Data
     */
    private Data $dataHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var UrlInterface
     */
    private UrlInterface $urlInterface;

    /**
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param UrlInterface $urlInterface
     * @param Data $dataHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoreLocatorRepository $storeLocatorRepository,
        UrlInterface $urlInterface,
        Data $dataHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder

    ){
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->urlInterface = $urlInterface;
        $this->dataHelper = $dataHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param string $urlKey
     * @return StoreLocatorInterface
     * @throws NoSuchEntityException
     */
    public function getItem(string $urlKey): StoreLocatorInterface
    {
        $data = $this->storeLocatorRepository->getByUrlKey($urlKey);
        return $data;
    }

    /**
     * @return StoreLocatorInterface[]
     */
    public function getAllItems(): array
    {
        $filter = $this->searchCriteriaBuilder->setCurrentPage(1)->create();
        $data = $this->storeLocatorRepository->getList($filter);
        return $data->getItems();
    }

    /**
     * @return string
     */
    public function getMediaPath(): string
    {
        return $this->urlInterface->getBaseUrl() . DirectoryList::MEDIA . "/";
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->dataHelper->getApiKey();
    }
}
