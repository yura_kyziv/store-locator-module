require(
    [
        'Magento_Ui/js/lib/validation/validator',
        'jquery',
        'mage/translate'
    ], function(validator, $){
        validator.addRule(
            'jsonValidationRule',
            function (value) {
                if(!value){
                    return true;
                }
                try {
                    JSON.parse(value);
                } catch (e) {
                    return false;
                }
                return true;
            },
            $.mage.__('Schedule does`t match the Json-format')
        );
    });
