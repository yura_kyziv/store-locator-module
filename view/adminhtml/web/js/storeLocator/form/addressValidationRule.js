require(
    [
        'Magento_Ui/js/lib/validation/validator',
        'jquery',
        'mage/translate'
    ], function(validator, $){
        validator.addRule(
            'addressValidationRule',
            function (value) {
                let country = $("select[name*='general[country]']").val();
                let city = $("input[name*='general[city]']").val();
                let address = value;
                let apiKey = $("#api_key").text();
                let mapAddress = encodeURIComponent(address + city + country);

                let urlApi = `https://maps.googleapis.com/maps/api/geocode/json?address=${mapAddress}&key=${apiKey}&isAjax=true`;
                var requestData = []

                $.ajax({
                    url: urlApi,
                    async: false,
                    dataType: 'json',
                    success: function (json) {
                        requestData = json;
                    }
                })
                if (requestData.status === "OK") {
                    requestData = requestData.results.slice(-1)[0];
                    if (requestData['geometry']['location_type'] .includes('RANGE_INTERPOLATED')) {
                        return false;
                    }
                    requestData = requestData['types'][0];
                    let errorPlaces = ['locality', 'neighborhood'];
                    return !errorPlaces.includes(requestData);
                }
                return false
            },
            $.mage.__('Incorrect address. Please check is correct country, city or address.')
        );
    });
