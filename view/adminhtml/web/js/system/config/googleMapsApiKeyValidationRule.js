define(
    [
        'jquery'
    ], function ($) {
        'use strict';
        return function (target) {
            $.validator.addMethod(
                'googleMapsApiKeyValidationRule',
                function (value) {
                    let urlApi = `https://maps.googleapis.com/maps/api/geocode/json?address=B-56%2BSector-64%2BNoida%2Bup%2Bindia&key=${value}&isAjax=true`;

                    var requestStatus = []

                    $.ajax({
                        url: urlApi,
                        async: false,
                        dataType: 'json',
                        success: function (json) {
                            requestStatus = json.status;
                        }
                    })

                    return requestStatus === "OK"
                },
                $.mage.__('Api key does`t valid')
            );
            return target;
        };
    });

