require([
    'jquery'
],
function ($) {
    let general = $(".section-config").first();
    general.change(function (){
        isEnable();
    })
    isEnable();

    function isEnable() {
        let isEnable = general.find("#mastering_storeLocator_general_enable").attr("selected", "").val();
        let sections = $(".section-config:not(:has(#mastering_storeLocator_general-head))");
        if (isEnable) {
            if(isEnable != 1){
                sections.hide();
            }else {
                sections.show();
            }
        }
    }
});
