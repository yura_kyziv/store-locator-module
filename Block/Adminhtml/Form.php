<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Block\Adminhtml;

/**
 * class for edit/create template form in admin
 */
class Form extends \Magento\Framework\View\Element\Template
{
}
