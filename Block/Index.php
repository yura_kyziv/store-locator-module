<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Mastering\StoreLocator\Model\ResourceModel\StoreLocator\Collection;
use Magento\Theme\Block\Html\Pager;

/**
 * class for index view page template
 */
class Index extends Template
{

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $storeLocatorCollection;

    /**
     * Page item limit
     */
    private const PAGE_LIMIT = 8;

    public function __construct(
        Template\Context  $context,
        CollectionFactory $storeLocatorCollection,
        array             $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeLocatorCollection = $storeLocatorCollection;
    }

    /**
     * @return $this|Index
     * @throws LocalizedException
     */
    protected function _prepareLayout(): Index
    {
        parent::_prepareLayout();
        $page_size = $this->getPagerCount();
        $page_data = $this->getCustomData();
        if ($this->getCustomData()) {
            $pager = $this->getLayout()->createBlock(
                Pager::class,
                'stores.index.pager'
            )
                ->setAvailableLimit($page_size)
                ->setShowPerPage(true)
                ->setCollection($page_data);
            $this->setChild('pager', $pager);
            $this->getCustomData();
        }
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCustomData(): Collection
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : self::PAGE_LIMIT;
        $collection = $this->storeLocatorCollection->create();
        $collection->setCurPage($page)->setPageSize($pageSize);
        return $collection;
    }

    /**
     * @return array
     */
    public function getPagerCount(): array
    {
        $minimum_show = self::PAGE_LIMIT;
        $page_array = [];
        $list_data = $this->storeLocatorCollection->create();
        $list_count = ceil(count($list_data->getData()));
        $show_count = $minimum_show + 1;
        if (count($list_data->getData()) >= $show_count) {
            $list_count = $list_count / $minimum_show;
            $page_nu = $total = $minimum_show;
            $page_array[$minimum_show] = $minimum_show;
            for ($x = 0; $x <= $list_count; $x++) {
                $total = $total + $page_nu;
                $page_array[$total] = $total;
            }
        } else {
            $page_array[$minimum_show] = $minimum_show;
            $minimum_show = $minimum_show + $minimum_show;
            $page_array[$minimum_show] = $minimum_show;
        }
        return $page_array;
    }

    /**
     * @return string
     */
    public function getPagerHtml(): string
    {
        return $this->getChildHtml('pager');
    }

}
