<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

interface StoreLocatorRepositoryInterface
{
    /**
     * Get item by id
     *
     * @param int $id
     * @return StoreLocatorInterface;
     */
    public function get(int $id): StoreLocatorInterface;

    /**
     * Get item by url key
     *
     * @param string $urlKey
     * @return StoreLocatorInterface;
     */
    public function getByUrlKey(string $urlKey): StoreLocatorInterface;

    /**
     * Get list items
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): StoreLocatorSearchResultInterface;

    /**
     * Save data
     *
     * @param StoreLocatorInterface $storeLocator
     * @return StoreLocatorInterface
     */
    public function save(StoreLocatorInterface $storeLocator): StoreLocatorInterface;

    /**
     * Delete item
     *
     * @param StoreLocatorInterface $workingHours
     * @return bool
     */
    public function delete(StoreLocatorInterface $workingHours): bool;

    /**
     * delete item by id
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}
