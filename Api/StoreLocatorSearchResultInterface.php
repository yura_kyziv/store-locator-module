<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api;

use \Magento\Framework\Api\SearchResultsInterface;

interface StoreLocatorSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Mastering\StoreLocator\Api\Data\StoreLocatorInterface[]
     */
    public function getItems();

    /**
     * @param \Mastering\StoreLocator\Api\Data\StoreLocatorInterface[]
     * @return void
     */
    public function setItems(array $items);
}
