<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Rest\StoreLocator;

use Mastering\StoreLocator\Api\StoreLocatorSearchResultInterface;

/**
 * Rest action
 */
interface GetListInterface
{

    /**
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(): StoreLocatorSearchResultInterface;

}
