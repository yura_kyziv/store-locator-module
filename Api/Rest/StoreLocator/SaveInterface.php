<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Rest\StoreLocator;

use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Rest action
 */
interface SaveInterface
{

    /**
     * @param StoreLocatorInterface $storeLocator
     * @return StoreLocatorInterface
     */
    public function save(StoreLocatorInterface $storeLocator): StoreLocatorInterface;
}
