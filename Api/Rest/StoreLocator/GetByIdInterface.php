<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Rest\StoreLocator;

use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Rest action
 */
interface GetByIdInterface
{

    /**
     * @param mixed $id
     * @return StoreLocatorInterface
     */
    public function getById($id): StoreLocatorInterface;
}
