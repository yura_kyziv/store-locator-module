<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Rest\StoreLocator;

/**
 * Rest action
 */
interface DeleteByIdInterface
{
    /**
     * @param mixed $id
     * @return bool
     */
    public function deleteById($id): bool;

}
