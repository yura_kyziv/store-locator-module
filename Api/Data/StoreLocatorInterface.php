<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Data;

/**
 * Interface Store Locator model
 */
interface StoreLocatorInterface
{
    /**
     * Id attribute name
     *
     * @var string
     */
    const ID = 'id';

    /**
     * Store name attribute name
     *
     * @var string
     */
    const STORE_NAME = 'store_name';

    /**
     * Description attribute name
     *
     * @var string
     */
    const DESCRIPTION = 'description';

    /**
     * Image attribute name
     *
     * @var string
     */
    const IMAGE = 'image';

    /**
     * Country attribute name
     *
     * @var string
     */
    const COUNTRY = 'country';

    /**
     * City attribute name
     *
     * @var string
     */
    const CITY = 'city';

    /**
     * Address attribute name
     *
     * @var string
     */
    const ADDRESS = 'address';

    /**
     * Schedule attribute name
     *
     * @var string
     */
    const SCHEDULE = 'schedule';

    /**
     * Longitude attribute name
     *
     * @var string
     */
    const LONGITUDE = 'longitude';

    /**
     * Latitude attribute name
     *
     * @var string
     */
    const LATITUDE = 'latitude';

    /**
     * Store url attribute name
     *
     * @var string
     */
    const URL_KEY= 'store_url_key';

    /**
     * Get id attribute
     *
     * @return mixed|null
     */
    public function getId();

    /**
     * Get id attribute
     *
     * @param mixed $value
     * @return $this
     */
    public function setId($value): StoreLocatorInterface;

    /**
     * Get store name attribute
     *
     * @return string
     */
    public function getStoreName(): string;

    /**
     * Set store name attribute
     *
     * @param string $storeName
     * @return void
     */
    public function setStoreName(string $storeName): void;

    /**
     * Get description attribute
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set description attribute
     *
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void;

    /**
     * Get image attribute
     *
     * @return string
     */
    public function getImage(): string;

    /**
     * Set image attribute
     *
     * @param string $image
     * @return void
     */
    public function setImage(string $image): void;

    /**
     * Get country attribute
     *
     * @return string
     */
    public function getCountry(): string;

    /**
     * Set country attribute
     *
     * @param string $country
     * @return void
     */
    public function setCountry(string $country): void;

    /**
     * Get city attribute
     *
     * @return string
     */
    public function getCity(): string;

    /**
     * Set city attribute
     *
     * @param string $city
     * @return void
     */
    public function setCity(string $city): void;

    /**
     * Get address attribute
     *
     * @return string
     */
    public function getAddress(): string;

    /**
     * Set address attribute
     *
     * @param string $address
     * @return void
     */
    public function setAddress(string $address): void;

    /**
     * Get schedule attribute
     *
     * @return string
     */
    public function getSchedule(): string;

    /**
     * Set schedule attribute
     *
     * @param string $schedule
     * @return void
     */
    public function setSchedule(string $schedule): void;

    /**
     * Get longitude attribute
     *
     * @return float
     */
    public function getLongitude(): float;

    /**
     * Set longitude attribute
     *
     * @param float $longitude
     * @return void
     */
    public function setLongitude(float $longitude): void;

    /**
     * Get latitude attribute
     *
     * @return float
     */
    public function getLatitude(): float;

    /**
     * Set latitude attribute
     *
     * @param float $latitude
     * @return void
     */
    public function setLatitude(float $latitude): void;

    /**
     * Get store url attribute
     *
     * @return string
     */
    public function getUrlKey(): string;

    /**
     * Set store url attribute
     *
     * @param string $urlKey
     * @return void
     */
    public function setUrlKey(string $urlKey): void;

}
