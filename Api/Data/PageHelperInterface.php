<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Data;

/**
 * Interface for page help config
 */
interface PageHelperInterface
{
    /**
     * url for module config page
     *
     * @var string
     */
    const ADMIN_CONFIG = 'adminhtml/system_config/edit/section/mastering_storeLocator';

    /**
     * url for edit page
     *
     * @var string
     */
    const URL_PATH_EDIT = 'mastering/store_locator/edit';

    /**
     * url for view
     *
     * @var string
     */
    const URL_PATH_STORES = 'stores/';
}
