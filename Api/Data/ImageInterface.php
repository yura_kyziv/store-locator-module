<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Api\Data;

/**
 * Interface for image config
 */
interface ImageInterface
{
    /**
     * Path to images
     *
     * @var string
     */
    public const IMAGE_PATH = 'storeLocator/image/';
}
