<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Ui\DataProvider\StoreLocator;

use Mastering\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;

/**
 * Store locator data provider
 */
class FormDataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * @return array
     */
    public function getDataSourseData(): array
    {
        return [];
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData(): array
    {
        $items = parent::getData();
        if(isset($items['items'][0]['image'])){
            $image = $items['items'][0]['image'];
            $imageName = $image;
            $imageUrl = $this->getMediaUrl() . $image;
            $items['items'][0]['image'] = array(
                array(
                    'name'  =>  $imageName,
                    'url'   =>  $imageUrl
                )
            );
        }
        return $items;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    private function getMediaUrl(): string
    {
        $_objectManager = ObjectManager::getInstance();
        $storeManager = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(UrlInterface::URL_TYPE_MEDIA );

        return $mediaUrl;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }
}

