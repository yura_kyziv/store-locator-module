<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface as UrlInterfaceHelp;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Thumbnails in store locator listing
 */
class Thumbnail extends Column {

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManagerInterface;


    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManagerInterface
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storeManagerInterface,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManagerInterface = $storeManagerInterface;
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource): array
    {
        foreach($dataSource["data"]["items"] as &$item) {

            if (isset($item['image'])) {
                $url = $this->storeManagerInterface->getStore()->getBaseUrl(UrlInterfaceHelp::URL_TYPE_MEDIA) . $item['image'];
                $item['image_src'] = $url;
                $item['image_alt'] = $item['id'];
                $item['image_link'] = $url;
                $item['image_orig_src'] = $url;

            }
        }
        return $dataSource;
    }
}
