<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Ui\Component\Control\StoreLocator;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Mastering\StoreLocator\Api\Data\PageHelperInterface;
use Magento\Framework\View\Element\UiComponent\Context;

/**
 * view button in store locator admin page
 */
class ViewButton extends Generic implements ButtonProviderInterface
{

    /**
     * @var UrlInterface
     */
    private UrlInterface $urlBuilder;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        Context $context,
        Registry $registry,
        UrlInterface $urlBuilder
    ){
        parent::__construct($context, $registry);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return array
     */
    public function getButtonData(): array
    {
        return [
            'label' => __('View'),
            'class' => 'action-secondary',
            'on_click' => sprintf("location.href = '%s';", $this->urlBuilder->getBaseUrl() . PageHelperInterface::URL_PATH_STORES),
            'sort_order' => 21,
        ];
    }
}

