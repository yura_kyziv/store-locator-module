<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Ui\Component\Control\StoreLocator;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * save button in store locator form
 */
class SaveButton extends Generic implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData():array
    {
        return [
            'label' => __('Save Store Locator'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 20,
        ];
    }
}

