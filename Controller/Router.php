<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private ActionFactory $actionFactory;

    /**
     * @var ResponseInterface
     */
    private ResponseInterface $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */

    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = explode('/', $request->getPathInfo());

        if (strpos($request->getPathInfo(), 'stores') !== false) {
            if ($identifier[2] == ''){
                $request->setModuleName('stores');
                $request->setControllerName('stores');
                $request->setActionName('index');
                return $this->actionFactory->create(Forward::class, ['request' => $request]);
            } else {
                $request->setModuleName('stores');
                $request->setControllerName('stores');
                $request->setActionName('show');
                $request->setParams([
                    'store_url_key' => $identifier[2],
                ]);
                return $this->actionFactory->create(Forward::class, ['request' => $request]);
            }
        }
        return null;
    }
}
