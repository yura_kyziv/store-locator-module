<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Stores;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\UrlInterface;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Store locator page controller
 */
class Show extends Action
{
    /**
     * @var PageFactory
     */
    private PageFactory $_pageFactory;

    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param UrlInterface $url
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        UrlInterface $url,
        StoreLocatorRepository $storeLocatorRepository,
        PageHelper $pageHelper
    ){
        $this->url = $url;
        $this->_pageFactory = $pageFactory;
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->pageHelper = $pageHelper;
        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $this->pageHelper->checkModuleEnableFrontend($this->_redirect, $this->_response);
        $urlKey = $this->_request->getParam('store_url_key');
        try {
            $this->storeLocatorRepository->getByUrlKey($urlKey);
        } catch (\Exception $e) {
            $this->pageHelper->redirectTo404($this->_redirect, $this->_response);
        }
        $page =  $this->_pageFactory->create();
        $page->getConfig()
            ->getTitle()
            ->set($this->storeLocatorRepository->getByUrlKey($urlKey)->getStoreName());
        return $page;
    }
}
