<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Stores;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Magento\Framework\App\Action\Action;

/**
 * Store locator page controller
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    private PageFactory $_pageFactory;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        PageHelper $pageHelper
    ){
        $this->_pageFactory = $pageFactory;
        $this->pageHelper = $pageHelper;
        return parent::__construct($context);
    }

    public function execute()
    {
        $this->pageHelper->checkModuleEnableFrontend($this->_redirect, $this->_response);
        $page =  $this->_pageFactory->create();
        $page->getConfig()
            ->getTitle()
            ->set(__('Stores'));
        return $page;
    }
}

