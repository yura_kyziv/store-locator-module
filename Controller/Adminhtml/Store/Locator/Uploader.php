<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Mastering\StoreLocator\Api\Data\ImageInterface;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Magento\Framework\Controller\ResultInterface;

/**
 * Admin store locator action for uploading image
 */
class Uploader extends Action
{
    /**
     * @var ImageUploader
     */
    protected ImageUploader $imageUploader;

    /**
     * @var Filesystem
     */
    protected Filesystem $filesystem;

    /**
     * @var File
     */
    protected File $fileIo;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param ImageUploader $imageUploader
     * @param Filesystem $filesystem
     * @param File $fileIo
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        ImageUploader $imageUploader,
        Filesystem $filesystem,
        File $fileIo,
        PageHelper $pageHelper
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->filesystem = $filesystem;
        $this->fileIo = $fileIo;
        $this->pageHelper = $pageHelper;
    }

    /**
     * Upload file controller action.
     *
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute(): ResultInterface
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        $imageUploadId = $this->getRequest()->getParam('param_name', 'image');

        $tmpDirectoryPath = ImageInterface::IMAGE_PATH;
        $mediaRootDir = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath() . $tmpDirectoryPath;
        if (!is_dir($mediaRootDir)) {
            $this->fileIo->mkdir($mediaRootDir, 0775);
        }
        $this->imageUploader->setBaseTmpPath($tmpDirectoryPath);
        $imageResult = $this->imageUploader->saveFileToTmpDir( $imageUploadId);

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($imageResult);
    }
}
