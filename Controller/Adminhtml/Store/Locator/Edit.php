<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Admin store locator index page action
 */
class Edit extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::system';

    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        StoreLocatorRepository $storeLocatorRepository,
        PageHelper $pageHelper
    ) {
        parent::__construct($context);
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->pageHelper = $pageHelper;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        $id = $this->getRequest()->getParam('id');
        try {
            $type = $this->storeLocatorRepository->get($id);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('Mastering_StoreLocator::mastering')
                ->addBreadcrumb(__('Edit Store locator'), __('Store Locator'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit Store Locator: %store_name', ['store_name' => $type->getStoreName()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Store locator with id "%value" does not exist.', ['value' => $id])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}

