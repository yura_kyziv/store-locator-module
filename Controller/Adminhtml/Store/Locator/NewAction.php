<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Admin store locator create new action
 */
class NewAction extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::system';

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        PageHelper $pageHelper
    ) {
        parent::__construct($context);
        $this->pageHelper = $pageHelper;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        /** @var Page $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->setActiveMenu('Mastering_StoreLocator::store_locator')
            ->addBreadcrumb(__('New Store Locator'), __('Store Locator'));
        $result->getConfig()->getTitle()->prepend(__('New Store Locator'));

        return $result;
    }
}


