<?php
declare(strict_types=1);

namespace  Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use  Mastering\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Mastering\StoreLocator\Model\StoreLocatorRepository;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Exception;


class Delete extends Action implements HttpPostActionInterface
{
    /**
     * @var CollectionFactory
     */
    public CollectionFactory $collectionFactory;

    /**
     * @var Filter
     */
    public Filter $filter;

    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StoreLocatorRepository $storeLocatorRepository,
        PageHelper $pageHelper
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->pageHelper = $pageHelper;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface | ResultInterface
     */
    public function execute()
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();

            foreach ($collection as $model) {
                $this->storeLocatorRepository->deleteById($model->getId());
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 store(s) have been deleted.', $collectionSize));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }

    /**
     * @return bool
     */
    public function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed('Mastering_StoreLocator::mastering');
    }
}
