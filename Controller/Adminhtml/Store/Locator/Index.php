<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;

/**
 * Admin store locator index page action
 */
class Index extends Action
{

    public const ADMIN_RESOURCE = "Magento_Backend::system";

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        PageHelper $pageHelper
    ) {
        parent::__construct($context);
        $this->pageHelper = $pageHelper;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Mastering_StoreLocator::store_locator')
            ->addBreadcrumb(__('Store Locator'), __('List'));
        $resultPage->getConfig()->getTitle()->prepend(__('Store Locator'));

        return $resultPage;
    }

}
