<?php
declare(strict_types=1);

namespace Mastering\StoreLocator\Controller\Adminhtml\Store\Locator;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Mastering\StoreLocator\Api\Data\StoreLocatorInterface;
use Mastering\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Mastering\StoreLocator\Helper\Data;
use Mastering\StoreLocator\Helper\StoreLocator\DataSave;
use Mastering\StoreLocator\Helper\StoreLocator\PageHelper;
use Mastering\StoreLocator\Model\StoreLocatorFactory;
use Exception;

/**
 * Admin store locator save action
 */
class Save extends Action implements HttpPostActionInterface
{

    /**
     * @var StoreLocatorRepositoryInterface
     */
    private StoreLocatorRepositoryInterface $storeLocatorRepository;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var Data
     */
    private Data $dataHelper;

    /**
     * @var PageHelper
     */
    private PageHelper $pageHelper;

    /**
     * @param Context $context
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param Data $dataHelper
     * @param  PageHelper $pageHelper
     */
    public function __construct(
        Context $context,
        StoreLocatorRepositoryInterface $storeLocatorRepository,
        StoreLocatorFactory $storeLocatorFactory,
        Data $dataHelper,
        PageHelper $pageHelper
    ) {
        parent::__construct($context);
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->dataHelper = $dataHelper;
        $this->pageHelper = $pageHelper;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $this->pageHelper->checkModuleEnableAdmin($this->_redirect, $this->_response);
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();

        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            $resultRedirect->setPath('*/*/new');
            return $resultRedirect;
        }

        try {
            $id = $requestData['general'][StoreLocatorInterface::ID];
            $storeLocator = $this->storeLocatorRepository->get($id);
        } catch (Exception $e) {
            $storeLocator = $this->storeLocatorFactory->create();
        }

        try {
            $dataSave = new DataSave($storeLocator, $this->dataHelper);
            $dataSave->saveData($requestData['general']);
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save'));
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }

        try {
            $this->storeLocatorRepository->save($storeLocator);
            $resultRedirect->setPath('*/*/');
            $this->messageManager->addSuccessMessage(__('Store Locator was saved.'));

        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save'));
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }
}



